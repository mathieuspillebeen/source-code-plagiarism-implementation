#!/bin/bash
while getopts p: option
do
case "${option}"
in
p) DIRPATH=${OPTARG};;
esac
done

FILES=$DIRPATH/*
UNZIPERRORMESSAGE="Unzip-errors: "
HTMLERRORMESSAGE="Students without HMTL: "
CSSERRORMESSAGE="Students without CSS: "

echo "Recreate the unzipped folders folder"
sudo rm -rf $DIRPATH/unzipped-folders

mkdir $DIRPATH/unzipped-folders

cd $DIRPATH
echo "Starting to unzip while keeping the names intact"
# unzip all folders and keep the names intact
for f in *.zip; do
  unzip "${f}" -d "unzipped-folders/${f%%.zip}"

  # weird if statement to check if the last command succeeded
  # The unzip command will fail when the files inside contain special unicode characters
  if [ $? -ne 0 ]; then
    UNZIPERRORMESSAGE="$UNZIPERRORMESSAGE, Chinese character detected with a foldername: $f Please unzip this folder manually, check where chinese characters exist, remove them, and zip it again before rerunning the script."
  fi
done
echo "Done unzipping!"

cd ../

# Remove the analysis before recreating it again
#find $DIRPATH -name _analysis -exec rm -rf {} \;
echo "recreating the _analysis folder!"
rm -r _analysis
mkdir _analysis
mkdir _analysis/HTML
mkdir _analysis/CSS
mkdir _analysis/JS

echo "Removing all __MACOSX directories"
find $DIRPATH/unzipped-folders -name __MACOSX -exec rm -rf {} \;

echo "Removing all reset.css files"
find $DIRPATH/unzipped-folders -iname reset.css -exec rm -rf {} \;

echo "Removing all normalize.css files"
find $DIRPATH/unzipped-folders -iname normalize.css -exec rm -rf {} \;

echo "Strip all spaces from folder and file names so that they don't trip up the script"
for ugly_named_studentfolder in "$DIRPATH"/unzipped-folders/*; do
  find "$ugly_named_studentfolder" -name "* *" -print0 | sort -rz | \
  while read -d $'\0' f; do mv -v "$f" "$(dirname "$f")/$(basename "${f// /_}")"; done
done

for studentfolder in "$DIRPATH"/unzipped-folders/*; do
  # Safe the name of the folder in a variable 
  FOLDERNAME=$(basename $studentfolder)

  # HTML
  # List all the html files of a student.
  STUDENTHTML=$(find "$studentfolder" -type f -iname "*.html")

  # Only if the variable is not empty
  if [ ! -z "$STUDENTHTML" ]
  then
    # Find the exact sames files again, but merge them in to a new file.
    find "$studentfolder" -type f -iname "*.html" -exec cat {} \; > _analysis/HTML/$FOLDERNAME.html
  else
    HTMLERRORMESSAGE="$HTMLERRORMESSAGE $studentfolder,"
  fi  
  
  # CSS
  # List all the CSS files of a student.
  # use -iname instead of -name, so that students that named their css file style.CSS, also get picked up.
  STUDENTCSS=$(find "$studentfolder" -type f -iname "*.css")

  # Only if the variable is not empty
  if [ ! -z "$STUDENTCSS" ]
  then
    # Find the exact sames files again, but merge them in to a new file.
    find "$studentfolder" -type f -iname "*.css"  -exec cat {} \; > _analysis/CSS/$FOLDERNAME.css
  else
    CSSERRORMESSAGE="$CSSERRORMESSAGE $studentfolder,"
  fi

  # JS
  # List all the JS files of a student.
  STUDENTJS=$(find "$studentfolder" -type f -iname "*.js")

  # Only if the variable is not empty
  if [ ! -z "$STUDENTJS" ]
  then
    # Find the exact sames files again, but merge them in to a new file.
    find "$studentfolder" -type f -iname "*.js"  -exec cat {} \; > _analysis/JS/$FOLDERNAME.js
  fi
done

# Remove all the unzipped folders so we have a clean start next time.
# Sudo is needed for students that changed the rights of their own folder before compressing.
# find "$DIRPATH"/unzipped-folders/* -type d -exec sudo rm -rf {} \;

echo ${UNZIPERRORMESSAGE}
echo ${HTMLERRORMESSAGE}
echo ${CSSERRORMESSAGE}

echo "run the following command to analyse the CSS or HTML respectively:"
echo "npx dolos run -f web -l css _analysis/CSS/*.css" 
echo "npx dolos run -f web -l html _analysis/HTML/*.html" 
