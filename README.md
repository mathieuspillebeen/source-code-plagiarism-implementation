# source-code-plagiarism-implementation

Firstly download all the submissions from speedgrader.
Next, unzip the freshly downloaded submissions.zip using your own tool and a folder with zip files inside will appear. Name this folder "inzendingen"
Next, download the shell script locally and put it next to the folder "inzendingen"
Next open up command line and cd to your folder.
Next excecute this:
```
sh analyse-html-css-js.sh -p inzendingen
```


The script should now create a _analysis-folder, where each student will receive a html and css file that will have all the content submitted concatenated in there.

Next you can run the analysis tool by excecuting:
for HTML
```
npx dolos run -f web -l html _analysis/HTML/*.html
```

for CSS: 
```
npx dolos run -f web -l css _analysis/CSS/*.css
```

In case of memory overuse: 
* check how large the largest file is that you want to check (if someone added a library to the css, it might overuse memory)
* Up the memory limit (replace 8192 by whatever)
export NODE_OPTIONS="--max-old-space-size=8192"


## why are the node_modules committed?
The dolos parser is a bit silly and thinks HTML and CSS shouldn't be supported, so in order for dolos to be able to do their thing, I hacked a package in node_modules, 
and changed in node_modules/@dodona/dolos/dist/lib/tokenizer/codeTokenizer.js
this line to include "css" and "html":
```
CodeTokenizer.supportedLanguages = ["c", "c-sharp", "bash", "java", "javascript", "python", "css", "html"];
```

So we have a non-npm versioned working tree of modules for html and css.

Haven't tried to run it without having the parsers installed globally.

